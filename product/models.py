from db import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship



#models of database

class Product(Base):
    __tablename__ = "product"    

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    price = Column(Integer)
    user_id = Column(Integer, ForeignKey("user.id"))

    buyer = relationship("User", back_populates="product")   #create relationship with user table
    

class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    email = Column(String)
    password = Column(String)

    product = relationship("Product", back_populates="buyer")   #create relationship with product table

from fastapi import APIRouter, Depends
import db, schemas, models
from typing import List
from sqlalchemy.orm import session
from fastapi.exceptions import HTTPException
from hashing import Hash


#for creating routing
router = APIRouter(
    prefix='/user', 
    tags=['Users']
)

#post->  create the name, email. and bcrypt pass 
@router.post("/",status_code= 201)
def create( request: schemas.User, db: session= Depends(db.get_db)):
   
    new_user = models.User(name= request.name, email= request.email, password= Hash.bcrypt(request.password)) #assigning data to fields
    db.add(new_user) #add in db
    db.commit()   #commit in db
    db.refresh(new_user)  #refresh db
    return new_user

#get-> retrieve all the data and show the name and email(bcz of response model)
@router.get("/",response_model=List[schemas.ShowUser])
def show_all(db: session= Depends(db.get_db)):
    return db.query(models.User).all()   #retrieving all the data with the help of sqlachemy query



#get with id{} -> retrive a particular data and show email and name
@router.get("/{id}", response_model=schemas.ShowUser)
def show_User(id: int, db: session = Depends(db.get_db)):
    user = db.query(models.User).filter(models.User.id == id).first()    #filter out the first comment with that id
    if not user:
        raise HTTPException(           #raise the exception when user with a particular id not found
             status_code=404,
             detail="Item not found",
        ) 
    return user



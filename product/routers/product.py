from fastapi import APIRouter, Depends
import db, schemas, models
from typing import List
from sqlalchemy.orm import session
from fastapi.exceptions import HTTPException

#for creating routing 
router = APIRouter(
    prefix='/product', 
    tags=['products']

)


#get-> retrieve all the data and show the name(bcz of response model)
@router.get("/", response_model=List[schemas.ShowProduct])
def show_all(db: session= Depends(db.get_db)):
    return db.query(models.Product).all()  #retrieving all the data with the help of sqlachemy query


#post->  create the name and price
@router.post('/', status_code= 201)
def create( request: schemas.Product, db: session= Depends(db.get_db)):
    new_product = models.Product(name= request.name, price= request.price, user_id = 1)  #assigning data to fields
    db.add(new_product)         #add in db
    db.commit()                 #commit in db
    db.refresh(new_product)    #refresh db
    return new_product


#get with id{} -> retrive a particular data and show name
@router.get("/{id}", response_model=schemas.ShowProduct)
def show_Employee(id: int, db: session = Depends(db.get_db)):
    product = db.query(models.Product).filter(models.Product.id == id).first()    #filter out the first comment with that id
    if not product:
        raise HTTPException(                             #raise the exception when product with a particular id not found
             status_code=404,
             detail="Item not found",
        ) 
    return product



#delete-> remove the product with a particular id
@router.delete("/{id}")
def delete(id:int, db: session = Depends(db.get_db)):
    product = db.query(models.Product).filter(models.Product.id == id)  #retrieving the data with the id
    if not product.first(): 
        raise HTTPException(          #raise exception when data not found
            status_code=404,
            detail="Item not found",
           
        )
    product.delete(synchronize_session=False)  #delete the data if found
    db.commit()  #comit the changes in database
    return "Deleted succesfully"



#put-> uodate the data with the help of id
@router.put("/{id}", status_code=202,)
def update(id:int, request:schemas.Product, db: session = Depends(db.get_db)):
    
    #retriving the data with id than updating it
    product = db.query(models.Product).filter(models.Product.id == id).update({"name" : request.name, "price" : request.price}, synchronize_session = False)

    if not product:                             #raise exception when d not found
        raise HTTPException(
            status_code=404,
            detail="Item not found",
           
        )
    
    db.commit() #commit the changes in db if data updated
    
    return "Updated succesfully"


from pydantic import BaseModel
from typing import Optional, List



#create pydantic models and extend it with BaseModel
class ProductBase(BaseModel):
    name:str
    price:int
    
#pydantic model for response model
class Product(ProductBase):
    class Config():
        orm_mode = True


#create pydantic models and extend it with BaseModel
class User(BaseModel):
    name:str
    email:str
    password:str

#pydantic model for response model
class ShowUser(BaseModel):
    name:str
    email:str
    product: List[Product] = []
    class Config():
        orm_mode = True


#pydantic model for response model
class ShowProduct(BaseModel):
    name:str
    buyer: ShowUser
    class Config():
        orm_mode = True

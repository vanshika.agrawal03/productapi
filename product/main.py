
from fastapi import FastAPI, Depends
from db import engine
import models
from routers import product, user


#create instance of fastapi
app = FastAPI()

#connecting with db
models.Base.metadata.create_all(bind=engine)


#create routing
app.include_router(product.router)
app.include_router(user.router)











